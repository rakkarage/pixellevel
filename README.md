# **PixelLevel** 🗺️

<img align="right" src="icon.png">

Pixel-perfect dungeon and environment sprites.

- 📦 <http://henrysoftware.itch.io/godot-pixel-level>
- 🌐 <http://rakkarage.github.io/PixelLevel>
- 📃 <http://guthub.com/rakkarage/PixelLevel>

[![.github/workflows/compress.yml](https://github.com/rakkarage/PixelLevel/actions/workflows/compress.yml/badge.svg)](https://github.com/rakkarage/PixelLevel/actions/workflows/deploy.yml)
[![.github/workflows/deploy.yml](https://github.com/rakkarage/PixelLevel/actions/workflows/deploy.yml/badge.svg)](https://github.com/rakkarage/PixelLevel/actions/workflows/deploy.yml)
